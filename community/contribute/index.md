---
title: Contribute to Kali
description:
icon:
date: 2019-12-11
type: post
weight: 100
author: ["gamb1t",]
tags: ["",]
keywords: ["",]
og_description:
---

## Kali Linux Mirror Contributions

Mirrors are always good to have. The more we have, and the faster they are, the better. For more information about becoming an official Kali Linux mirror, check out our [Mirror Policy Page](/docs/community/kali-linux-mirrors/).

## Kali Linux Packaging

With the number of tools out there that users want to see in Kali, there is no way we can get them all on our own. A perk of us being on GitLab is that we can easily accept packages submitted by users, to learn more check out our [Public Packaging page](/docs/development/public-packaging/).

## Kali Linux Community

The official [Kali Linux Forums](https://forums.kali.org), [Bug Tracker](https://bugs.kali.org/), and [IRC channel](https://www.kali.org/docs/community/kali-linux-irc-channel/) are all great places to contribute by helping others in the community. We do have doc pages on the [forums](https://www.kali.org/docs/community/kali-linux-community-forums/) and the [bug tracker](https://www.kali.org/docs/community/submitting-issues-kali-bug-tracker/) that we encourage you to check out first.

## Kali Linux Documentation

You can contribute by helping us out right here! If there are any typos you notice, ideas you have, anything at all that could be beneficial then you can submit a request for the changes [over on GitLab](https://gitlab.com/kalilinux/documentation/kali-docs).